-- Example of a repeatable migration
-- Add the following users to the databsae if they don't already exist
WITH user_data(name, username, email, password) AS (
    VALUES
    ( 'Abhinav Kishore', 'abhi.kishore95', 'abhi.kishore95@gmail.com', 'abhi_secret' ),
    ( 'Adishwar Rishi', 'adiswa123', 'adiswa123@gmail.com', 'adi_secret' )
)
INSERT INTO users (name, username, email, password)
SELECT
    d.name,
    d.username,
    d.email,
    d.password
FROM user_data d
WHERE NOT EXISTS (
    SELECT 1 FROM users u2
    WHERE d.name = u2.name
        AND d.username = u2.username
        AND d.email = u2.email
)
