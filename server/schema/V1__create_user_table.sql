-- User Table
CREATE TABLE IF NOT EXISTS Users (
	id SERIAL,
	created_at TIMESTAMP NULL DEFAULT NULL,
	updated_at TIMESTAMP NULL DEFAULT NULL,
	name VARCHAR(255) NULL DEFAULT NULL,
	username VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,

	-- table constraints
	PRIMARY KEY (id),
	UNIQUE (username),
	UNIQUE (email)
);
