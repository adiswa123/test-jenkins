import * as bodyParser from 'body-parser';
import { Application } from 'express';
import * as session from 'express-session';

export const setSession = (app: Application): Application => {
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(
        session({
            resave: true,
            saveUninitialized: true,
            secret: process.env.SESSION_SECRET
        })
    );
    return app;
};
