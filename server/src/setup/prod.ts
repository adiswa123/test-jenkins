import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import { Application } from 'express';
import * as flash from 'express-flash';
import * as session from 'express-session';
import expressValidator = require('express-validator');
import * as lusca from 'lusca';
import * as logger from 'morgan';

export const setProd = (app: Application): Application => {
    app.use(compression());
    app.use(logger('dev'));
    app.use(expressValidator());
    app.use(flash());
    app.use(lusca.xframe('SAMEORIGIN'));
    app.use(lusca.xssProtection(true));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(
        session({
            resave: true,
            saveUninitialized: true,
            secret: process.env.SESSION_SECRET
        })
    );
    return app;
};
