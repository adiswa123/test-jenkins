import * as fs from 'fs';
import * as path from 'path';
import * as Sequelize from 'sequelize';
import { ConnectionInterface } from '../api/types';
import './env-config';

import { getModels } from '../models/db.tables';

const sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.POSTGRES_USER,
    process.env.POSTGRES_PASSWORD,
    {
        host: 'localhost',
        dialect: 'postgres',
        define: {
            timestamps: false
        }
    } as Sequelize.Options
);

const connection: ConnectionInterface = {
    models: getModels(sequelize),
    sequelize
};

// Establish associations.
Object.keys(connection.models).forEach((modelname: string) => {
    if ('associate' in (connection.models as any)[modelname]) {
        (connection.models as any)[modelname].associate(connection.models);
    }
});

export default connection;
