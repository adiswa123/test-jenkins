import * as dotenv from 'dotenv';
import * as path from 'path';

/**
 * Expose private variables in process.env.*
 */
export const setupDotEnvConfig: (envPath: string) => { [name: string]: string } = (
    envPath: string
) => {
    const result = dotenv.config({ path: envPath });
    if (result.error) {
        throw result.error;
    }
    return result.parsed;
};

console.log(path.join(__dirname, '../../../.env'));
export default setupDotEnvConfig(path.join(__dirname, '../../../.env'));
