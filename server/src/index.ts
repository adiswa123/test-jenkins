import { graphiqlExpress, graphqlExpress } from 'apollo-server-express';
import * as bodyParser from 'body-parser';
import * as errorHandler from 'errorhandler';
import * as express from 'express';
import * as path from 'path';
import GraphQLSchema from './api/schema';
import sequelize from './setup/database';
import './setup/env-config';
import { setProd } from './setup/prod';
import { setSession } from './setup/session';

/**
 * Create server.
 */
const app = express();

/**
 * Express configurations.
 */
setSession(app);
setProd(app);
app.use(express.static(path.join(__dirname, '../../../client/dist')));
app.set('port', process.env.PORT || 3000);
app.set('views', [path.join(__dirname, '../../../client/dist')]);
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

/**
 * Primary app routes.
 */
const graphQLOptions = {
    schema: GraphQLSchema,
    context: { connection: sequelize.models }
};
const graphIQLOptions = {
    endpointURL: '/graphql'
};
app.use('/graphql', bodyParser.json(), graphqlExpress(graphQLOptions));
app.use('/graphiql', graphiqlExpress(graphIQLOptions));
app.use('/healthcheck', require('./routers/healthcheck'));
app.use('/*', require('./routers/client'));

/**
 * Error Handler. Provides full stack - remove for production
 */
app.use(errorHandler());

/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
    console.log(
        '  App is running at http://localhost:%d in %s mode',
        app.get('port'),
        app.get('env')
    );
    console.log('  Press CTRL-C to stop\n');
});

module.exports = app;
