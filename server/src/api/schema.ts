import { makeExecutableSchema } from 'graphql-tools';
import { RootMutation, RootQuery, RootSchema } from './entities/Root';
import UserEntity, { mutationAddUser, resolveUser, resolveUsers } from './entities/User';

/**
 * Complete schema - assembled through modular definitions for each entity.
 */
const typeDefs = [UserEntity, RootQuery, RootMutation, RootSchema];
/**
 * Complete resolver map - for nested queries.
 */
const resolvers = {
    RootQuery: {
        user: resolveUser,
        users: resolveUsers
    },
    RootMutation: {
        addUser: mutationAddUser
    }
};

export default makeExecutableSchema({
    typeDefs,
    resolvers
});
