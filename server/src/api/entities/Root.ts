/**
 * All valid queries on the GraphQL server.
 */
const RootQuery = `
type RootQuery {
  user(id: Int!): User
  users: [User]
}`;

/**
 * All valid mutations on the GraphQL server.
 */
const RootMutation = `
type RootMutation {
  addUser(input: UserInput): User
}`;

/**
 * Overall schema.
 */
const RootSchema = `
schema {
  query: RootQuery
  mutation: RootMutation
}`;

export { RootQuery, RootMutation, RootSchema };
