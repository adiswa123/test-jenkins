/* tslint:disable:interface-over-type-literal */

import { usersAttribute } from '../../models/db';
import { GraphQLContext } from '../types';

// Schema.
const User = `
    type User {
        id: ID!
        name: String
        email: String
        username: String
        created_at: String,
        updated_at: String,
    }
`;

const UserInput = `
    input UserInput {
        name: String!
        username: String!
        email: String!
        password: String!
    }
`;

// Resolvers.
const resolveUser = (obj: any, args: usersAttribute, context: GraphQLContext) => {
    return context.connection.users.findById(args.id);
};
const resolveUsers = (obj: any, args: usersAttribute, context: GraphQLContext) => {
    return context.connection.users.findAll();
};

type mutationAddUserInput = {
    input: usersAttribute;
};

// Mutations.
const mutationAddUser = (obj: any, args: mutationAddUserInput, context: GraphQLContext) => {
    return context.connection.users.create(args.input);
};

// Export all schema and resolvers.
export { resolveUser, resolveUsers, mutationAddUser };
export default () => [User, UserInput];
