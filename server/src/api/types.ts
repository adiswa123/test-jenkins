import { Sequelize } from 'sequelize';
import { usersModel } from '../models/db';
import { ITables } from '../models/db.tables';

export interface ConnectionInterface {
    models: ITables;
    sequelize: Sequelize;
}

export interface GraphQLContext {
    connection: ITables;
}
