/* tslint:disable:no-console */
import connection from '../../setup/database';

connection.sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
        process.exit();
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
        process.exit();
    });
