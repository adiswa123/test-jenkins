## Postgres Docker Container Bootstrap files

After the entrypoint calls initdb to create the default postgres user and database, it will run any `*.sql` files and source any `*.sh` scripts found in that directory to do further initialization before starting the service.

**Files will be run in sorted name order**
