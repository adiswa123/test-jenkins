#!/usr/bin/env bash
echo "
Starting database boostrap
=========================

Creating as user - ${POSTGRES_USER}
Creating database - ${DB_NAME}
"
psql -v ON_ERROR_STOP=1 --username $POSTGRES_USER <<-EOSQL
	CREATE DATABASE $DB_NAME;
	GRANT ALL PRIVILEGES ON DATABASE $DB_NAME TO $POSTGRES_USER;
EOSQL