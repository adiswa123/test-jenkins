import * as fs from 'fs';
import * as path from 'path';

/**
 * This script reads all the files from the content directory (above)
 * and sets up flyway schema for each respective directory.
 */
const isDirectory = (dirName: string): boolean => dirName.indexOf('.') === -1;

// Precompute which schema version we are up to.
const schemaFiles = fs.readdirSync(path.resolve('./schema'));
const schemaNumber = parseInt(schemaFiles.sort().pop()[1], 10) + 1;
console.log('-- SCHEMA VERSION (NEXT):', schemaNumber);
