// tslint:disable
import * as Sequelize from 'sequelize';

// table: users
export interface usersAttribute {
    id: number;
    createdAt?: Date;
    updatedAt?: Date;
    name?: string;
    username: string;
    email: string;
    password: string;
}
export interface usersInstance extends Sequelize.Instance<usersAttribute>, usersAttribute {}
export interface usersModel extends Sequelize.Model<usersInstance, usersAttribute> {}
