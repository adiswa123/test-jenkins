/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import { DataTypes } from 'sequelize';
import { usersInstance, usersAttribute } from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
    return sequelize.define<usersInstance, usersAttribute>(
        'users',
        {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,
                field: 'id'
            },
            createdAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: 'created_at'
            },
            updatedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: 'updated_at'
            },
            name: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: 'NULL',
                field: 'name'
            },
            username: {
                type: DataTypes.STRING,
                allowNull: false,
                unique: true,
                field: 'username'
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false,
                unique: true,
                field: 'email'
            },
            password: {
                type: DataTypes.STRING,
                allowNull: false,
                field: 'password'
            }
        },
        {
            tableName: 'users'
        }
    );
};
