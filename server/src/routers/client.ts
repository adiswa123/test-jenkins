import { Request, Response } from 'express';

const router = require('express').Router();
const path = require('path');

/**
 * Controller logic.
 */
export let getFrontendRoute = (req: Request, res: Response) => {
    res.render('index.html');
};

/**
 * API Routes.
 */
router.get('*', getFrontendRoute);

/**
 * Exported router.
 */
module.exports = router;
