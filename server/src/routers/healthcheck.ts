import { Request, Response } from 'express';

const router = require('express').Router();
const path = require('path');

/**
 * Controller logic.
 */
export let getHealthCheck = (req: Request, res: Response) => {
    res.send('pong');
};

/**
 * API Routes.
 */
router.get('/ping', getHealthCheck);

/**
 * Exported router.
 */
module.exports = router;
