import * as compression from 'compression';
import { Application } from 'express';
import * as flash from 'express-flash';
import expressValidator = require('express-validator');
import * as lusca from 'lusca';
import * as logger from 'morgan';

export const setProd = (app: Application): Application => {
    app.use(compression());
    app.use(logger('dev'));
    app.use(expressValidator());
    app.use(flash());
    app.use(lusca.xframe('SAMEORIGIN'));
    app.use(lusca.xssProtection(true));
    return app;
};
