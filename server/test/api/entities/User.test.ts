import 'mocha';
import * as sinon from 'sinon';
import { SinonStub } from 'sinon';

import { resolveUser } from '../../../src/api/entities/User';
import { ConnectionInterface } from '../../../src/api/types';
import { usersAttribute } from '../../../src/models/db';
import { boostrapDatabase, expect } from '../../setup';

describe('User.ts resolvers', () => {
    let connection: ConnectionInterface = null;

    before(async () => {
        connection = await boostrapDatabase();
    });

    after(async () => {
        await connection.sequelize.close();
    });

    describe('resolveUser()', () => {
        const sandbox = sinon.createSandbox();
        let findByIdStub: SinonStub;

        beforeEach(() => {
            findByIdStub = sandbox.stub(connection.models.users, 'findById');
        });

        afterEach(() => {
            sandbox.restore();
        });

        it('should call findById', () => {
            resolveUser({}, { id: 1 } as usersAttribute, { connection: connection.models });
            expect(findByIdStub).to.have.been.calledOnce;
        });

        it('should call findById with the id provided', () => {
            resolveUser({}, { id: 1 } as usersAttribute, { connection: connection.models });
            expect(findByIdStub).to.have.been.calledWithExactly(1);
        });
    });
});
