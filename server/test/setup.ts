import * as chai from 'chai';
import * as fs from 'fs';
import 'mocha';
import * as path from 'path';
import * as Sequelize from 'sequelize';
import * as sinonChai from 'sinon-chai';
import * as app from '../src';
import { ConnectionInterface } from '../src/api/types';
import '../src/setup/env-config';

import { getModels } from '../src/models/db.tables';

// setup sinon API inside chai
chai.use(sinonChai);

export const expect = chai.expect;

export const boostrapDatabase = async () => {
    const sequelize = new Sequelize(process.env.DB_NAME, process.env.POSTGRES_USER, null, {
        host: 'localhost',
        dialect: 'sqlite',
        define: {
            timestamps: false
        },
        logging: false
    } as Sequelize.Options);

    const connection: ConnectionInterface = {
        models: getModels(sequelize),
        sequelize
    };

    // Sync all models
    const allSyncPromises = [];
    for (const model of Object.values(connection.models)) {
        allSyncPromises.push(model.sync());
    }

    // Establish associations.
    Object.keys(connection.models).forEach((modelname: string) => {
        if ('associate' in (connection.models as any)[modelname]) {
            (connection.models as any)[modelname].associate(connection.models);
        }
    });

    try {
        await Promise.all(allSyncPromises);
    } catch (e) {}

    return connection;
};
