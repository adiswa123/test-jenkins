const path = require('path');
const webpack = require('webpack');
const dotenv = require('dotenv');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

const envParsed = dotenv.config({ path: path.join(__dirname, '.env') });
if (envParsed.error) {
    throw envParsed.error;
}
const env = envParsed.parsed;

module.exports = {
    watch: true,
    mode: 'development',
    entry: {
        'app': './src/js/main.tsx',
    },
    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        alias: {
            '@assets': path.resolve('./src/static'),
            '@app': path.resolve('./src/js'),
            '@test': path.resolve('./test'),
        },
        extensions: [".ts", ".tsx", ".js", ".json"]
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
        chunkFilename: '[name].bundle.js',
        publicPath: 'http://localhost:8080/js/',
    },
    devtool: 'source-map',
    devServer: {
        hot: true, // Tell the dev-server we're using HMR
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
            "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
        }
    },
    optimization: {
        noEmitOnErrors: true,
        namedModules: true,
    },
    plugins: [
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        new HtmlWebpackPlugin({
            alwaysWriteToDisk: true,
            inject: true,
            filename: 'index.html',
            template: './src/static/html/index.ejs',
            title: 'A Boilerplate for World Peace'
        }),
        new HtmlWebpackHarddiskPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new ForkTsCheckerWebpackPlugin({
            tslint: true,
        })
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.tsx?$/,
                use: [
                    'babel-loader',
                    {
                        loader: 'ts-loader',
                        options: {
                            // Disable type checker - we will use it in the fork plugin.
                            transpileOnly: true
                        }
                    }
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
                    'image-webpack-loader'
                ]
            },
            {
                enforce: "pre",
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'source-map-loader'
            }
        ]
    }
};
