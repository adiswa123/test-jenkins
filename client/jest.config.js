module.exports = {
    collectCoverage: true,
    collectCoverageFrom: ['!**/node_modules/**', 'src/js/**/*.{js,jsx,ts,tsx}'],
    coverageDirectory: 'coverage',
    globals: {
        'ts-jest': {
            useBabelrc: true
        }
    },
    moduleFileExtensions: ['js', 'json', 'jsx', 'node', 'ts', 'tsx'],
    moduleNameMapper: {
        '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
            '<rootDir>/test/__mocks__/assetMock.js'
    },
    setupFiles: ['./test/setupTests.ts'],
    testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?)$',
    transform: {
        '^.+\\.(tsx?|ts?)$': 'ts-jest'
    }
};
