import * as React from 'react';
import styled from 'styled-components';
import Grid from '../../components/Grid/Grid';
import { IndexPageContainerProps } from '../IndexPageContainer/IndexPageContainer';

/**
 * Presentational components.
 */
export const Header = styled.h1`
    padding-top: 320px;
    color: black;
`;

/**
 * Container component.
 */
interface Props extends IndexPageContainerProps {}
interface State {}

export default class IndexPage extends React.Component<Props, State> {
    public render() {
        const { data: { loading, users } } = this.props;
        return (
            <Grid>
                <Header>Hello; a peaceful hello, to you (Y).</Header>
                {loading ? null : <p>Currently: {users.length} users.</p>}
            </Grid>
        );
    }
}
