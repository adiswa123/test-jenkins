import * as React from 'react';
import { compose, graphql, GraphqlQueryControls } from 'react-apollo';
import { UserListQuery } from '../../queries/schema';
import { GQLUserList } from './IndexPageContainerQueries';

/**
 * Interfaces for all containers with the following hydrations.
 */
export interface IndexPageContainerProps {
    data: UserListQuery & GraphqlQueryControls;
}

/**
 * GraphQL setup - query and HOC declaration.
 */
export default graphql<{}, UserListQuery>(GQLUserList);
