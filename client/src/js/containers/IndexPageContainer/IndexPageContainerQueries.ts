import gql from 'graphql-tag';

export const GQLUserList = gql`
    fragment UserDetails on User {
        id
        name
    }

    query UserList {
        users {
            ...UserDetails
        }
    }
`;
