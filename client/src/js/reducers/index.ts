import { routerReducer } from 'react-router-redux';
import { combineReducers } from 'redux';
import { StoreTree } from '../constants/initialState';

export default function createReducer() {
    return combineReducers<StoreTree>({
        routing: routerReducer
    });
}
