import { ApolloClient } from 'apollo-client';
import { History } from 'history';
import { routerMiddleware } from 'react-router-redux';
import { applyMiddleware, compose, createStore, Reducer, Store } from 'redux';

import { default as appInitialState, StoreTree } from '../constants/initialState';
import createReducer from '../reducers';

declare const window: any;

export function configureStore(history: History, client: ApolloClient<any>): Store<StoreTree> {
    const reducers = createReducer();
    const middleware = routerMiddleware(history);
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const store = createStore<StoreTree>(
        reducers,
        {},
        composeEnhancers(applyMiddleware(middleware))
    );

    if (module.hot) {
        module.hot.accept('../reducers', () => {
            const nextRootReducer: Reducer<StoreTree> = require('../reducers');
            store.replaceReducer(createReducer());
        });
    }

    return store;
}
