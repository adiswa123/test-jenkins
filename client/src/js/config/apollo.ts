import { InMemoryCache } from 'apollo-cache-inmemory';
import ApolloClient from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { ErrorHandler, onError } from 'apollo-link-error';
import { createHttpLink } from 'apollo-link-http';

export interface ApolloClientConfig {
    uri: string;
}

export default class CustomApolloClient extends ApolloClient<any> {
    constructor(config: ApolloClientConfig) {
        // Apollo no longer relies on Redux, hence the following.
        const apolloCache = new InMemoryCache();
        const apolloHttpLink = createHttpLink({
            uri: config.uri,
            credentials: 'include'
        });

        const errorHandler: ErrorHandler = ({ graphQLErrors, networkError }) => {
            if (graphQLErrors.length > 0) {
                graphQLErrors.map(({ message, locations, path }) =>
                    console.log(
                        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
                    )
                );
            }
            if (networkError) {
                console.log(`[Network error]: ${networkError}`);
            }
        };
        const apolloLink = ApolloLink.from([onError(errorHandler), apolloHttpLink]);

        super({
            cache: apolloCache,
            link: apolloLink,
            connectToDevTools: true
        });
    }
}
