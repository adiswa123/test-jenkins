import { Config, Google, load } from 'webfontloader';

const GoogleFontConfig: Google = {
    families: ['Carrois Gothic']
};
const WebFontConfig: Config = {
    google: GoogleFontConfig
};

export const configureFonts = (): void => {
    load(WebFontConfig);
};
