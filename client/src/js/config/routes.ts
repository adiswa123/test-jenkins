import * as React from 'react';
import { RouteConfig, RouteConfigComponentProps } from 'react-router-config';

import IndexPage from '../containers/IndexPage/IndexPage';
import hydrateWithUsers from '../containers/IndexPageContainer/IndexPageContainer';

const routes: RouteConfig[] = [
    {
        component: hydrateWithUsers(IndexPage),
        routes: [
            {
                path: '/',
                exact: false,
                component: hydrateWithUsers(IndexPage)
            }
        ]
    }
];

export default routes;
