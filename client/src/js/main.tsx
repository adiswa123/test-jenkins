import { createBrowserHistory as createHistory, History } from 'history';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { Store } from 'redux';
import ApolloClient from './config/apollo';

import { configureStore } from './config/store';
import { StoreTree } from './constants/initialState';
import App from './index';

/**
 * Require interfaces - for static type-checks.
 */
interface RequireImportFonts {
    configureFonts: () => void;
}
interface RequireImport {
    default: any;
}
interface RequireImportStore {
    configureStore: (history: History, client: ApolloClient) => Store<StoreTree>;
}

/**
 * Bootstrap CSS and Google Webfonts.
 */
require('normalize.css');
require('@assets/css/bootstrap.css');
require<RequireImportFonts>('./config/fonts').configureFonts();

/**
 * Bootstrap React SPA.
 */
const client = new ApolloClient({ uri: 'http://localhost:3000/graphql' });
const history = createHistory();
const store = configureStore(history, client);

ReactDOM.render(
    <AppContainer>
        <App store={store} history={history} client={client} />
    </AppContainer>,
    document.getElementById('app-container') as HTMLElement
);

/**
 * React Hot Module Replacement (react-hot-loader) setup.
 */
if (module.hot) {
    module.hot.accept('./index', () => {
        const nextConfigureStore: RequireImportStore = require('./config/store');
        const newStore = nextConfigureStore.configureStore(history, client);
        const NextApp = require<RequireImport>('./').default;
        ReactDOM.render(
            <AppContainer>
                <NextApp store={newStore} history={history} client={client} />
            </AppContainer>,
            document.getElementById('app-container')
        );
    });
}
