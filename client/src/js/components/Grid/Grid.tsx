import * as React from 'react';
import styled from 'styled-components';

interface Props {
    className?: string;
}
interface State {}

class Grid extends React.Component<Props, State> {
    public render() {
        return <div className={this.props.className}>{this.props.children}</div>;
    }
}

const GridStyled = styled(Grid)`
    width: 100vw;
    height: 100vh;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI';
    text-align: center;
    display: flex;
    flex-direction: column;
`;

export default GridStyled;
export { Grid };
