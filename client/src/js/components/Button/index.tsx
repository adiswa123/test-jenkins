import * as React from 'react';
import styled from 'styled-components';

interface Props {
    children: React.ReactNode;
}
interface State {}

const StyledButton = styled.button`
    padding: 20px;
    outline: none;
    border: 1px solid #ccc;
    background-color: white;
    color: black;
    transition: all 300ms ease-in-out;
    border-radius: 10px;
    &:hover {
        background-color: #efefef;
        color: black;
        cursor: pointer;
    }
`;
export default class Button extends React.PureComponent<Props, State> {
    public render() {
        return <StyledButton>{this.props.children}</StyledButton>;
    }
}
