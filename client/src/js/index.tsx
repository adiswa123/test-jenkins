import ApolloClient from 'apollo-client';
import { History } from 'history';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import * as ReactGA from 'react-ga';
import { IntlProvider } from 'react-intl';
import { Provider } from 'react-redux';
import { Route } from 'react-router';
import { renderRoutes } from 'react-router-config';
import { Router } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import { Store } from 'redux';

import routes from './config/routes';
import { StoreTree } from './constants/initialState';

const intlMessages = require('./i18n/en.json');

interface Props {
    history: History;
    store: Store<StoreTree>;
    client: ApolloClient<any>;
}
interface State {}

export default class App extends React.PureComponent<Props, State> {
    public componentWillMount() {
        ReactGA.initialize('<GA-Key>');
    }

    public componentDidMount() {
        const { history } = this.props;
        history.listen((location, action) => {
            ReactGA.set({ page: location.pathname });
            ReactGA.pageview(location.pathname);
        });
    }

    public render() {
        return (
            <Provider store={this.props.store}>
                <ApolloProvider client={this.props.client}>
                    <IntlProvider locale="en" messages={intlMessages}>
                        <ConnectedRouter history={this.props.history}>
                            {renderRoutes(routes)}
                        </ConnectedRouter>
                    </IntlProvider>
                </ApolloProvider>
            </Provider>
        );
    }
}
