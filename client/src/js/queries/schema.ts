/* tslint:disable */
//  This file was automatically generated and should not be edited.

export interface UserListQuery {
    users: Array<{
        id: string;
        name: string | null;
    } | null> | null;
}

export interface UserDetailsFragment {
    id: string;
    name: string | null;
}
