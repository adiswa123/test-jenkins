import { shallow } from 'enzyme';
import * as React from 'react';

import IndexPage, { Header } from '@app/containers/IndexPage/IndexPage';

describe('IndexPage.tsx', () => {
    const userDataLoading = {
        loading: true
    } as any;
    const userData = {
        loading: false,
        users: Array(105)
    } as any;

    test('When users are loading, only the header should be rendered', () => {
        const page = shallow(<IndexPage data={userDataLoading} />);
        expect(page.find(Header)).toHaveLength(1);
        expect(page.find('p')).toHaveLength(0);
    });

    test('When users have loaded, the number of users should be displayed', () => {
        const page = shallow(<IndexPage data={userData} />);
        expect(page.find(Header)).toHaveLength(1);
        expect(page.find('p')).toHaveLength(1);
        expect(page.find('p').text()).toContain(userData.users.length);
    });
});
