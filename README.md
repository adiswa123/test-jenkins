# Express-TypeScript Boilerplate

The following boilerplate establishes a full-stack [TypeScript](https://www.typescriptlang.org) boilerplate.

It utilises [Express](expressjs.com) on the backend and a stack inspired by [Redux Without Profanity](https://tonyhb.gitbooks.io/redux-without-profanity/) on the frontend.

Remaining action items include: 

* Adding a DB layer to the backend.
* Adding testing stack (including coverage) for the backend.
* Adding testing stack (including coverage) for the frontend.
* Integrating the above with Bitbucket Pipelines.